//
//  PDPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String?
    var dateOfBirth : String!
    var dateToday : String!
   // var doctorName: String!
    var clinicName : String!
    
    var fullName: String {
        return initial!.characters.count > 0 ? firstName + " " + initial! + " " + lastName : firstName + " " + lastName
    }
    
    var address: String!
    var city: String!
    var state: String!
    var zipCode: String!
    
    
    ////NEW PATIENT FORM
    
    var newPatientAddress : String!
    var newPatientCity : String!
    var newPatientState  : String!
    var newPatientZipcode : String!
    var newPatientDayPhone : String!
    var newPatientOtherPhone : String!
    var newPatientPreviousName : String!
    var newPatientMRNumber : String!
    
    var newPatientProviderName : String!
    var newPatientProviderLocation : String!
    var newPatientProviderAddress : String!
    var newPatientProviderFax : String!
    var newPatientProviderCity : String!
    var newPatientProviderState : String!
    var newPatientProviderZipcode : String!
    
    var newPatientRequestorName : String!
    var newPatientRequestorApptDate : String!
    var newPatientRequestorAddress : String!
    var newPatientRequestorFax : String!
    var newPatientRequestorCity : String!
    var newPatientRequestorState : String!
    var newPatientRequestorZipcode : String!
    
    var newPatientRecordsConcerning : String!
    var newPatientInformationTagArray : NSArray!
    var newPatientInformationOther : String!
    var newPatientCommunicationTagArray : NSArray!
    var newPatientReleaseTag : NSArray!
    var newPatientReleaseOther : String!
    
    var newPatientMail : String!
    var newpatientPickUp : String!
    var newPatientLegalRepresentative : String!
    var newPatientSignature : UIImage!
    
    var consentParticipation : Int!
    var consentSignatureImage : UIImage!
    var consentRelationship : String!
    var consentWitness : String!
    var consentDisabilityTag : Int!
    var consentName : String!
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
}

