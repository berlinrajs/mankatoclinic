//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kNewPatientForm = "AUTHORIZATION TO RELEASE AND DISCLOSE PATIENT INFORMATION"
//let kPrivacyPractice = "ACKNOWLEDGEMENT OF RECEIPT OF NOTICE OF PRIVACY PRACTICE"
//let kSlidingFeeApp = "SLIDING FEE APPLICATION"
//let kClientInformation = "CLIENT INFORMATION FORM"
//let kIncomeAffirmation = "INCOME AFFIRMATION FORM"
//let kSlidingFeeEligibility = "SLIDING FEE SCALE ELIGIBILITY FORM"
let kAcknowledgementForm  = "CONSENT FOR SERVICES AND INSURANCE HIPAA ACKNOWLEDGEMENT"

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(forms : [Forms]) -> Void) {
        var formObj = [Forms]()
        let forms = [kNewPatientForm,kAcknowledgementForm]
        formObj = getFormObjects(forms, isSubForm: false)
        completion(forms : formObj)
    }
    

    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = idx
            formObj.formTitle = form
            formList.append(formObj)
        }
        return formList
    }
    
}
