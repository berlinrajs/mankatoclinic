//
//  ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: PDViewController {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var labelVersion : UILabel!
    
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.showAlert), name: kFormsCompletedNotification, object: nil)
        
        if let text = NSBundle.mainBundle().infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Forms.getAllForms { (forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
        }
    }
    
    @IBAction func buttonActionLogout(sender: AnyObject) {
        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
        buttonLogout.hidden = true
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerate() {
            if form.isSelected == true {
                selectedForms.append(form)
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
            let patient = PDPatient(forms: selectedForms)
            
            patient.dateToday = labelDate.text
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
            
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func showAlert() {
        viewAlert.hidden = false
        viewShadow.hidden = false
    }
    
    @IBAction func buttonActionOk(sender: AnyObject) {
        
        viewAlert.hidden = true
        viewShadow.hidden = true
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let form = formList[indexPath.row]
        form.isSelected = !form.isSelected
        tableView.reloadData()
}
}



extension HomeViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formList.count > 0 ? formList.count : 0
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 0 ? 80 : 42
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellMainForm", forIndexPath: indexPath) as! FormsTableViewCell
        let form = formList[indexPath.row]
        cell.labelFormName.text = form.formTitle
        cell.imageViewCheckMark.hidden = !form.isSelected
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        return cell
    }
    
}
