//
//  ConsentForm2ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/9/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentForm2ViewController: PDViewController {

    @IBOutlet weak var radioPatient : RadioButton!
    @IBOutlet weak var signature1 : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var textfieldWitness : UITextField!
    @IBOutlet weak var viewLabel : UIView!
  //  @IBOutlet weak var textfieldName : UITextField!
    var disabilityTag : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if radioPatient.selectedButton == nil {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !signature1.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if !labelDate1.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if ((radioPatient.selectedButton.tag == 2) && (textfieldRelationship.isEmpty || textfieldWitness.isEmpty || !labelDate2.dateTapped)){
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            patient.consentSignatureImage = signature1.signatureImage()
            patient.consentRelationship = textfieldRelationship.isEmpty ? "" : textfieldRelationship.text!
            patient.consentWitness = textfieldWitness.isEmpty ? "" : textfieldWitness.text!
            patient.consentDisabilityTag = disabilityTag
            patient.consentName = ""
            let consent3 = self.storyboard!.instantiateViewControllerWithIdentifier("Consent3VC") as! ConsentForm3ViewController
            consent3.patient = self.patient
            self.navigationController?.pushViewController(consent3, animated: true)
        }
    }
    
    @IBAction func radioPatientAction (sender : RadioButton){
        if sender.tag == 1 {
            textfieldWitness.userInteractionEnabled = false
            textfieldRelationship.userInteractionEnabled = false
            viewLabel.userInteractionEnabled = false
            textfieldRelationship.alpha = 0.5
            textfieldWitness.alpha = 0.5
            viewLabel.alpha = 0.5
            textfieldWitness.text = ""
            textfieldRelationship.text = ""
        }else{
            textfieldWitness.userInteractionEnabled = true
            textfieldRelationship.userInteractionEnabled = true
            viewLabel.userInteractionEnabled = true
            textfieldRelationship.alpha = 1.0
            textfieldWitness.alpha = 1.0
            viewLabel.alpha = 1.0
            
            AlertDisability.sharedInstance.showPopUp({ (tag) in
                if tag == 0{
                    self.disabilityTag = 0
                    sender.deselectAllButtons()
                    self.textfieldWitness.userInteractionEnabled = false
                    self.textfieldRelationship.userInteractionEnabled = false
                    self.viewLabel.userInteractionEnabled = false
                    self.textfieldRelationship.alpha = 0.5
                    self.textfieldWitness.alpha = 0.5
                    self.viewLabel.alpha = 0.5
                    self.textfieldWitness.text = ""
                    self.textfieldRelationship.text = ""

                }else{
                    self.disabilityTag = tag
                }
            })
        }
    }
    
}

extension ConsentForm2ViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}