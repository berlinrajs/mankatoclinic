//
//  ConsentForm3ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/9/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentForm3ViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var radioParticipation : RadioButton!
    @IBOutlet weak var signatureImageView : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelRelationship : UILabel!
    @IBOutlet weak var labelWitness : UILabel!
    @IBOutlet weak var radioDisability : RadioButton!
    @IBOutlet weak var labelName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData()  {
        labelPatientName.text = patient.fullName
        radioParticipation.setSelectedWithTag(patient.consentParticipation)
        signatureImageView.image = patient.consentSignatureImage
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelRelationship.text = patient.consentRelationship
        labelWitness.text = patient.consentWitness
        radioDisability.setSelectedWithTag(patient.consentDisabilityTag)
        labelName.text = patient.clinicName
    }
}
