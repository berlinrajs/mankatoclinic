//
//  ConsentForm1ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/9/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentForm1ViewController: PDViewController {
    
    @IBOutlet weak var radioParticipation : RadioButton!
    var showBackButton : Bool!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.buttonBack?.hidden = !showBackButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        if radioParticipation.selectedButton == nil{
            let alert = Extention.alert("PLEASE CHOOSE YOUR PARTICIPATION IN HOME CARE HEALTH CONSENT")
            self.presentViewController(alert, animated: true, completion: nil)

        }else{
            patient.consentParticipation = radioParticipation.selectedButton.tag
            let consent2 = self.storyboard!.instantiateViewControllerWithIdentifier("Consent2VC") as! ConsentForm2ViewController
            consent2.patient = self.patient
            self.navigationController?.pushViewController(consent2, animated: true)
        }
        
    }
}
