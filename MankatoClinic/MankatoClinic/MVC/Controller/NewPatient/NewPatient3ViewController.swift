//
//  NewPatient3ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient3ViewController: PDViewController {

    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldApptDate : UITextField!
    @IBOutlet weak var textfieldFax : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)
        DateInputView.addDatePickerForTextField(textfieldApptDate, minimumDate: NSDate(), maximumDate: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        self.view.endEditing(true)
        if textfieldName.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldFax.isEmpty && !textfieldFax.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE THE VALID FAX NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            patient.newPatientRequestorName = textfieldName.text!
            patient.newPatientRequestorApptDate = textfieldApptDate.isEmpty ? "" : textfieldApptDate.text!
            patient.newPatientRequestorFax = textfieldFax.isEmpty ? "" : textfieldFax.text!
            patient.newPatientRequestorAddress = textfieldAddress.text!
            patient.newPatientRequestorCity = textfieldCity.text!
            patient.newPatientRequestorState = textfieldState.text!
            patient.newPatientRequestorZipcode = textfieldZipcode.text!
            let new4VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient4VC") as! NewPatient4ViewController
            new4VC.patient = self.patient
            self.navigationController?.pushViewController(new4VC, animated: true)
        }
    }

}

extension NewPatient3ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldFax {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldZipcode{
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
