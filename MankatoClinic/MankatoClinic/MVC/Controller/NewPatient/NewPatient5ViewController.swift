//
//  NewPatient5ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient5ViewController: PDViewController {
    
    @IBOutlet weak var buttonCheckmark1 : UIButton!
    @IBOutlet weak var buttonCheckmark2 : UIButton!
    @IBOutlet weak var signatureView1 : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
//    @IBOutlet weak var textfieldMail : UITextField!
//    @IBOutlet weak var textfieldPickup : UITextField!
    @IBOutlet weak var textfieldLegalRepresentative : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
       // DateInputView.addDatePickerForTextField(textfieldPickup)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        self.view.endEditing(true)
        if !buttonCheckmark1.selected || !buttonCheckmark2.selected {
            let alert = Extention.alert("PLEASE SELECT THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !signatureView1.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else{
            patient.newPatientMail = ""
            patient.newpatientPickUp = ""
            patient.newPatientLegalRepresentative = textfieldLegalRepresentative.isEmpty ? "" : textfieldLegalRepresentative.text
            patient.newPatientSignature = signatureView1.signatureImage()
            let newFormVC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatientFormVC") as! NewPatientFormViewController
            newFormVC.patient = self.patient
            self.navigationController?.pushViewController(newFormVC, animated: true)

        }
    }
    
    @IBAction func onCheckmarkButtonAction (sender : UIButton){
        sender.selected = !sender.selected
    }
}

extension NewPatient5ViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

