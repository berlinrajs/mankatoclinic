//
//  NewPatient4ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient4ViewController: PDViewController {

    @IBOutlet weak var textfieldRecordsConcerning : UITextField!
    @IBOutlet weak var radioButtonReason : RadioButton!
    var arrayInformationTag : NSMutableArray = NSMutableArray()
    var arrayCommunicationTag : NSMutableArray = NSMutableArray()
    var informationOther : String = ""
    var reasonForReleaseOther : String = ""
    var arrayReleaseTag : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        self.view.endEditing(true)
        if textfieldRecordsConcerning.isEmpty || arrayReleaseTag.count == 0 || arrayInformationTag.count == 0 || arrayCommunicationTag.count == 0{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            patient.newPatientRecordsConcerning = textfieldRecordsConcerning.text!
            patient.newPatientReleaseTag = arrayReleaseTag
            patient.newPatientReleaseOther = reasonForReleaseOther
            patient.newPatientInformationTagArray = arrayInformationTag
            patient.newPatientInformationOther = informationOther
            patient.newPatientCommunicationTagArray = arrayCommunicationTag
            
            let new5VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient5VC") as! NewPatient5ViewController
            new5VC.patient = self.patient
            self.navigationController?.pushViewController(new5VC, animated: true)
        }
    }
    
    @IBAction func onInformationDisclosedAction (sender : UIButton){
        sender.selected = !sender.selected
        if arrayInformationTag.containsObject(sender.tag){
            arrayInformationTag.removeObject(sender.tag)
        }else{
            arrayInformationTag.addObject(sender.tag)
        }
        
        if sender.selected && sender.tag == 10{
            PopupTextField.sharedInstance.showWithTitle("PLEASE SPECIFY", placeHolder: "TYPE HERE", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.informationOther = textField.text!
                }else{
                    self.informationOther = ""
                    sender.selected = false
                    self.arrayInformationTag.removeObject(sender.tag)
                }
                
            })
        }
    }
    
    @IBAction func onCommunicationButtonAction (sender : UIButton){
        sender.selected = !sender.selected
        if arrayCommunicationTag.containsObject(sender.tag){
            arrayCommunicationTag.removeObject(sender.tag)
        }else{
            arrayCommunicationTag.addObject(sender.tag)
        }
    }

    @IBAction func onReasonForReleaseButtonAction (sender : UIButton){
        sender.selected = !sender.selected
        if arrayReleaseTag.containsObject(sender.tag){
            arrayReleaseTag.removeObject(sender.tag)
        }else{
            arrayReleaseTag.addObject(sender.tag)
        }
        
        if sender.selected && sender.tag == 6{
            PopupTextField.sharedInstance.showWithTitle("PLEASE SPECIFY", placeHolder: "TYPE HERE", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.reasonForReleaseOther = textField.text!
                }else{
                    self.reasonForReleaseOther = ""
                    sender.selected = false
                    self.arrayReleaseTag.removeObject(sender.tag)
                }
            })
        }
    }
}

extension NewPatient4ViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
