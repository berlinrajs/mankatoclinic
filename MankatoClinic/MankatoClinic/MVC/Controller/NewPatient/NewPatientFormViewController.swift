//
//  NewPatientFormViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/8/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatientFormViewController: PDViewController {
    @IBOutlet weak var labelMRNumber : UILabel!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDayPhone : UILabel!
    @IBOutlet weak var labelOtherPhone : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZipcode : UILabel!
    @IBOutlet weak var labelDOB : UILabel!
    @IBOutlet weak var labelPreviousName : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    
    @IBOutlet weak var labelProviderName : UILabel!
    @IBOutlet weak var labelProviderLocation : UILabel!
    @IBOutlet weak var labelProviderAddress : UILabel!
    @IBOutlet weak var labelProviderFax : UILabel!
    @IBOutlet weak var labelProviderCity : UILabel!
    @IBOutlet weak var labelProviderState : UILabel!
    @IBOutlet weak var labelProviderZipCode : UILabel!
    
    @IBOutlet weak var labelRequestorName : UILabel!
    @IBOutlet weak var labelRequestorApptDate : UILabel!
    @IBOutlet weak var labelRequestorAddress : UILabel!
    @IBOutlet weak var labelRequestorFax : UILabel!
    @IBOutlet weak var labelRequestorCity : UILabel!
    @IBOutlet weak var labelRequestorState : UILabel!
    @IBOutlet weak var labelRequestorZipCode : UILabel!
    
    @IBOutlet weak var labelRecordsConcerning : UILabel!
    @IBOutlet var arrayButtonsInformation : [UIButton]!
    @IBOutlet var arrayButtonsCommunication : [UIButton]!
    @IBOutlet var arrayButtonRelease : [UIButton]!
    @IBOutlet weak var labelInformationOther : UILabel!
    @IBOutlet weak var labelReleaseOther : UILabel!
    
    @IBOutlet weak var imageViewSign : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelLegalRepresentative : UILabel!
    @IBOutlet weak var labelCopies : UILabel!
    @IBOutlet weak var labelPickUp : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadData() {
        labelMRNumber.text  = patient.newPatientMRNumber
        labelName.text = patient.fullName
        labelDayPhone.text = patient.newPatientDayPhone
        labelOtherPhone.text = patient.newPatientOtherPhone
        labelCity.text = patient.newPatientCity
        labelState.text = patient.newPatientState
        labelZipcode.text = patient.newPatientZipcode
        labelDOB.text = patient.dateOfBirth
        labelPreviousName.text = patient.newPatientPreviousName
        labelAddress.text = patient.newPatientAddress
        
        labelProviderName.text = patient.newPatientProviderName
        labelProviderLocation.text = patient.newPatientProviderLocation
        labelProviderAddress.text = patient.newPatientProviderAddress
        labelProviderFax.text = patient.newPatientProviderFax
        labelProviderCity.text = patient.newPatientProviderCity
        labelProviderState.text = patient.newPatientProviderState
        labelProviderZipCode.text = patient.newPatientProviderZipcode
        
        labelRequestorName.text = patient.newPatientRequestorName
        labelRequestorApptDate.text = patient.newPatientRequestorApptDate
        labelRequestorAddress.text = patient.newPatientRequestorAddress
        labelRequestorFax.text = patient.newPatientRequestorFax
        labelRequestorCity.text = patient.newPatientRequestorCity
        labelRequestorState.text = patient.newPatientRequestorState
        labelRequestorZipCode.text = patient.newPatientRequestorZipcode
        
        labelRecordsConcerning.text = patient.newPatientRecordsConcerning
        for btn in arrayButtonsInformation{
            btn.selected = patient.newPatientInformationTagArray.containsObject(btn.tag)
        }
        for btn in arrayButtonsCommunication{
            btn.selected = patient.newPatientCommunicationTagArray.containsObject(btn.tag)
        }
        for btn in arrayButtonRelease {
            btn.selected = patient.newPatientReleaseTag.containsObject(btn.tag)
        }
        labelInformationOther.text = patient.newPatientInformationOther
        labelReleaseOther.text = patient.newPatientReleaseOther
        
        imageViewSign.image = patient.newPatientSignature
        labelDate.text = patient.dateToday
        labelLegalRepresentative.text = patient.newPatientLegalRepresentative
        labelCopies.text = patient.newPatientMail
        labelPickUp.text = patient.newpatientPickUp
    }

}
