//
//  NewPatient2ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient2ViewController: PDViewController {
    
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldLocation : UITextField!
    @IBOutlet weak var textfieldFax : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        self.view.endEditing(true)
        if textfieldName.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldFax.isEmpty && !textfieldFax.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE THE VALID FAX NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            patient.newPatientProviderName = textfieldName.text!
            patient.newPatientProviderLocation = textfieldLocation.isEmpty ? "" : textfieldLocation.text!
            patient.newPatientProviderFax = textfieldFax.isEmpty ? "" : textfieldFax.text!
            patient.newPatientProviderAddress = textfieldAddress.text!
            patient.newPatientProviderCity = textfieldCity.text!
            patient.newPatientProviderState = textfieldState.text!
            patient.newPatientProviderZipcode = textfieldZipcode.text!
            let new3VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient3VC") as! NewPatient3ViewController
            new3VC.patient = self.patient
            self.navigationController?.pushViewController(new3VC, animated: true)
        }
    }
}

extension NewPatient2ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldFax {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldZipcode{
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
