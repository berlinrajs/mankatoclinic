//
//  NewPatient1ViewController.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient1ViewController: PDViewController {
    
    @IBOutlet weak var textFieldAddress : UITextField!
    @IBOutlet weak var textFieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldDayPhone : UITextField!
    @IBOutlet weak var textfieldOtherPhone : UITextField!
    @IBOutlet weak var textfieldPreviousName : UITextField!
    @IBOutlet weak var textfieldMRNumber : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textfieldState)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        self.view.endEditing(true)
        if textFieldAddress.isEmpty || textFieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || textfieldDayPhone.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldDayPhone.text!.isPhoneNumber || (!textfieldOtherPhone.isEmpty && !textfieldOtherPhone.text!.isPhoneNumber) {
            let alert = Extention.alert("PLEASE THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldMRNumber.isEmpty && textfieldMRNumber.text?.characters.count != 8 {
            let alert = Extention.alert("PLEASE THE VALID MR NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            patient.newPatientAddress = textFieldAddress.text!
            patient.newPatientCity = textFieldCity.text!
            patient.newPatientState = textfieldState.text!
            patient.newPatientZipcode = textfieldZipcode.text!
            patient.newPatientDayPhone = textfieldDayPhone.text!
            patient.newPatientOtherPhone = textfieldOtherPhone.isEmpty ? "" : textfieldOtherPhone.text!
            patient.newPatientPreviousName = textfieldPreviousName.isEmpty ? "" : textfieldPreviousName.text!
            patient.newPatientMRNumber = textfieldMRNumber.isEmpty ? "" : textfieldMRNumber.text!
            let new2VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient2VC") as! NewPatient2ViewController
            new2VC.patient = self.patient
            self.navigationController?.pushViewController(new2VC, animated: true)
        }
        
    }
}

extension NewPatient1ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldDayPhone || textField == textfieldOtherPhone{
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldZipcode{
            return textField.formatZipCode(range, string: string)
        }else if textField == textfieldMRNumber{
            return textField.formatMRNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
