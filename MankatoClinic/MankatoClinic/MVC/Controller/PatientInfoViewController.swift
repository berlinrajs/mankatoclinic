//
//  PatientInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var textFieldInitial: PDTextField!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldPrefferedName: PDTextField!
    @IBOutlet weak var buttonDoctorsList : UIButton!
    
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewDoctorList: PDTableView!
    @IBOutlet weak var labelDoctorName: UILabel!
    
    
    @IBOutlet weak var constraintClinicTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewClinicList: PDTableView!
    @IBOutlet weak var labelClinicName: UILabel!

    var clinicData : NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let plist = NSBundle.mainBundle().pathForResource("Clinic", ofType: "plist")
//        clinicData = NSDictionary(contentsOfFile: plist!)
//        let arrayClinic = clinicData?.allKeys.sort({ (obj1, obj2) -> Bool in
//            let state1 = obj1 as! String
//            let state2 = obj2 as! String
//            return state1 < state2
//        }) as! [String]


        datePicker.maximumDate = NSDate()
        DateInputView.addDatePickerForTextField(textFieldDateOfBirth, minimumDate: nil, maximumDate: NSDate())
        
//        tableViewDoctorList.layer.cornerRadius = 4.0
//        tableViewDoctorList.delegatePDTableView = self
//        self.tableViewDoctorList.arrayValues = []
//        self.tableViewDoctorList.reloadData()
//        self.tableViewDoctorList.showsVerticalScrollIndicator = true
//        
//        tableViewClinicList.layer.cornerRadius = 4.0
//        tableViewClinicList.delegatePDTableView = self
//        self.tableViewClinicList.arrayValues = arrayClinic
//        self.tableViewClinicList.reloadData()
//        self.tableViewClinicList.showsVerticalScrollIndicator = true
//        
        
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
//        if labelDoctorName.text == "SELECT A DOCTOR *" {
//            let alert = Extention.alert("PLEASE SELECT A DOCTOR")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldDateOfBirth.isEmpty {
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let date = dateFormatter.dateFromString(textFieldDateOfBirth.text!.capitalizedString)
            if date == nil {
                let alert = Extention.alert("PLEASE ENTER VALID DATE")
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                patient.clinicName = "MANKATO CLINIC"//labelClinicName.text
               // patient.doctorName = labelDoctorName.text
                patient.firstName = textFieldFirstName.text
                patient.lastName = textFieldLastName.text
                patient.dateOfBirth = textFieldDateOfBirth.text
                patient.initial = textFieldInitial.isEmpty ? "" : textFieldInitial.text
                self.gotoNextForm(true)
            }
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textFieldDateOfBirth.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
//    @IBAction func buttonActionSelectDoctor(sender: AnyObject) {
//        if labelClinicName.text == "SELECT A CLINIC *"{
//            let alert = Extention.alert("PLEASE SELECT ANY CLINIC")
//            self.presentViewController(alert, animated: true, completion: nil)
//              return
//        }
//        
//        if constraintTableViewHeight.constant == 0 {
//            self.view.layoutIfNeeded()
//            constraintTableViewHeight.constant = 6 * (70.0 * screenSize.height/1024.0)
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        } else {
//            self.view.layoutIfNeeded()
//            constraintTableViewHeight.constant = 0
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        }
//    }
//    
//    @IBAction func buttonActionSelectClinic (sender : UIButton){
//        if constraintClinicTableViewHeight.constant == 0 {
//            buttonDoctorsList.userInteractionEnabled = false
//            self.view.layoutIfNeeded()
//            constraintClinicTableViewHeight.constant = 6 * (70.0 * screenSize.height/1024.0)
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        } else {
//            buttonDoctorsList.userInteractionEnabled = true
//            self.view.layoutIfNeeded()
//            constraintClinicTableViewHeight.constant = 0
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        }
//    }
}


//extension PatientInfoViewController : PDTableViewDelegate {
//    
//    func selectedValue(value: String, id: Int, table: UITableView) {
//        buttonDoctorsList.userInteractionEnabled = true
//        if table == tableViewDoctorList{
//            labelDoctorName.text = value.uppercaseString
//            self.view.layoutIfNeeded()
//            constraintTableViewHeight.constant = 0
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        }else{
//            labelClinicName.text = value.uppercaseString
//            self.view.layoutIfNeeded()
//            constraintClinicTableViewHeight.constant = 0
//            self.tableViewDoctorList.arrayValues = clinicData[value] as? [String]
//            self.tableViewDoctorList.reloadData()
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//      }
//    }
//}
