//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: PDPatient!
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed (sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "People's Center Health Services", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if self.pdfView != nil {
                    pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
                
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    func gotoNextForm(showBackButton : Bool) {
        if isFromPreviousForm {
            patient.selectedForms.removeFirst()
        }
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kNewPatientForm){
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient1VC") as! NewPatient1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kAcknowledgementForm){
            let consent1 = self.storyboard!.instantiateViewControllerWithIdentifier("Consent1VC") as! ConsentForm1ViewController
            consent1.patient = self.patient
            consent1.showBackButton = showBackButton
            self.navigationController?.pushViewController(consent1, animated: true)
        }

        else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
}
