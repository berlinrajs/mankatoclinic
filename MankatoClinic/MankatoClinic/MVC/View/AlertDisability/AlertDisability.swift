//
//  AlertDisability.swift
//  MankatoClinic
//
//  Created by Bala Murugan on 6/9/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AlertDisability: UIView {

    @IBOutlet weak var radioButtonDisability : RadioButton!
    
    var completion:((Int)->Void)?
    
    static var sharedInstance : AlertDisability {
        let instance :  AlertDisability = NSBundle.mainBundle().loadNibNamed("AlertDisability", owner: nil, options: nil).first as! AlertDisability
        return instance
    }
    
    func showPopUp(completion: (tag: Int) -> Void)  {
        self.completion = completion
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func onOKButtonPressed (sender : UIButton){
        self.completion!(radioButtonDisability.selectedButton == nil ? 0 : radioButtonDisability.selectedButton.tag)
        self.removeFromSuperview()
    }
}

